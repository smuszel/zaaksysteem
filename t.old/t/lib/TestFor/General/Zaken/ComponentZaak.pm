package TestFor::General::Zaken::ComponentZaak;

use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Object::Model;
use Zaaksysteem::Search::ZQL;

sub setup : Test(startup) {
    my $self = shift;

    $self->{model} = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );
}

sub zs_zaken_componentzaak_deletion_errors : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        {
            my $case = $zs->create_case_ok();
            $case->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                $case->deletion_errors(),
                [],
                'No deletion errors for deletable case',
            );
            ok($case->can_delete(), 'API thinks case is deleteable too');
        }
        {
            my $case1 = $zs->create_case_ok();
            my $case2 = $zs->create_case_ok();
            my $case3 = $zs->create_case_ok();

            $case1->update(
                {
                    milestone => 2,
                }
            );
            $case2->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                    milestone => 2,
                    pid => $case1->id,
                }
            );
            $case3->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                    milestone => 2,
                    pid => $case2->id,
                }
            );

            is_deeply(
                $case1->deletion_errors(),
                [
                    'Zaak is nog niet afgehandeld',
                    'Bewaartermijn is niet verstreken'
                ],
                'Deletion errors for a grandchild with undeleteable grandparent and parent',
            );
            is_deeply(
                $case2->deletion_errors(),
                [
                    'Hoofdzaak ' . $case1->id . ' is nog niet afgehandeld'
                ],
                'Deletion errors for a child with undeleteable parent but deleteable child',
            );
            is_deeply(
                $case3->deletion_errors(),
                [
                    "Bewaartermijn van hoofdzaak " . $case2->id . " niet verstreken",
                    "Hoofdzaak " . $case1->id . " is nog niet afgehandeld",
                ],
                'Deletion errors for a grandchild with undeleteable grandparent and parent',
            );
        }

        {
            my $case1 = $zs->create_case_ok();
            my $case2 = $zs->create_case_ok();
            my $case3 = $zs->create_case_ok();

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );
            $case2->update({pid => $case1->id, milestone => 2});
            $case3->update(
                {
                    pid => $case1->id,
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->now()->add(days => 7),
                    milestone => 3,
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_errors()} ],
                [
                    'Bewaartermijn van deelzaak ' . $case3->id . ' niet verstreken',
                    'Deelzaak ' . $case2->id . ' is nog niet afgehandeld',
                ],
                'Deletion errors for case with undeleteable children',
            );
        }
    }, 'Case deletion errors');
}

sub zs_zaken_componentzaak_deletion_warnings : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $case1 = $zs->create_case_ok();
        my $case2 = $zs->create_case_ok();
        my $case3 = $zs->create_case_ok();

        $case1->update(
            {
                status => 'resolved',
                afhandeldatum => DateTime->now(),
                vernietigingsdatum => DateTime->new(
                    year      => 1955,
                    month     => 11,
                    day       => 12,
                    hour      => 22,
                    minute    => 4,
                    time_zone => 'US/Pacific',
                ),
            }
        );
        $case2->update({pid => $case1->id, milestone => 2});
        $case3->update(
            {
                pid => $case1->id,
                afhandeldatum => DateTime->now(),
                vernietigingsdatum => DateTime->now()->add(days => 7),
                milestone => 3,
            }
        );

        $schema->resultset('CaseRelation')->create(
            {
                case_id_a   => $case1->id,
                case_id_b   => $case2->id,
                order_seq_a => 1,
                order_seq_b => 1,
                type_a      => 'umpteenth cousin, thrice removed',
                type_b      => 'umpteenth cousin, thrice removed',
            }
        );
        $schema->resultset('CaseRelation')->create(
            {
                case_id_a   => $case1->id,
                case_id_b   => $case3->id,
                order_seq_a => 2,
                order_seq_b => 2,
                type_a      => 'umpteenth cousin, thrice removed',
                type_b      => 'umpteenth cousin, thrice removed',
            }
        );

        TODO: {
            local $TODO = "Object deletion changes may have triggered extra warnings";

            my $ok = is_deeply(
                [sort @{ $case1->deletion_warnings }],
                [
                    'Bewaartermijn voor gerelateerde zaak '
                        . $case3->id
                        . ' is nog niet verstreken',
                    'Gerelateerde zaak '
                        . $case2->id
                        . ' is nog niet afgehandeld',
                ],
                'Deletion warnings'
            );
            if (!$ok) {
                diag explain $case1->deletion_warnings();
            }
        }

        {
            require Zaaksysteem::Object::Types::TestObject;

            my $case1 = $zs->create_case_ok();
            my $object = Zaaksysteem::Object::Types::TestObject->new();

            my $case_obj = $case1->object_data;
            $object->add_relation(
                Zaaksysteem::Object::Relation->new(
                    related_object_id => $case_obj->uuid,
                    related_object_type => 'case',
                    relationship_name_a => 'thief',
                    relationship_name_b => 'victim',
                    blocks_deletion => 1,
                )
            );
            my $saved_object = $self->{model}->save(object => $object);

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_warnings()} ],
                [
                    "Object '" .$saved_object->type . "(...". substr($saved_object->id, -6) . ")' is nog aan deze zaak gekoppeld"
                ],
                'Deletion warnings for case with delete-blocking object relation',
            );
        }

        {
            require Zaaksysteem::Object::Types::ScheduledJob;

            my $case1 = $zs->create_case_ok();
            my $object = Zaaksysteem::Object::Types::ScheduledJob->new(
                job             => 'CreateCase',
                next_run        => DateTime->now(),
                interval_period => 'once',
                runs_left       => 1,
            );

            my $case_obj = $case1->object_data;
            $object->add_relation(
                Zaaksysteem::Object::Relation->new(
                    related_object_id => $case_obj->uuid,
                    related_object_type => 'case',
                    relationship_name_a => 'thief',
                    relationship_name_b => 'victim',
                    blocks_deletion => 1,
                )
            );
            my $saved_object = $self->{model}->save(object => $object);

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_warnings()} ],
                [
                    "Taak 'scheduled_job(..." . substr($saved_object->id, -6) . ")' is nog niet voltooid"
                ],
                'Deletion warnings for case with delete-blocking object relation to ScheduledJob',
            );
        }
    }, 'Case deletion warnings');
}

sub zs_zaken_regression_zs7831 : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $zaak = $zs->create_case_ok;

        my $phase_count = $zaak->zaaktype_node_id->zaaktype_statuses->count;
        my $allocation_actions = $zaak->case_actions->search({ type => 'allocation' });

        is $phase_count, $allocation_actions->count, 'default case has allocation actions';
        is $phase_count, $zaak->case_actions->count, 'default case actions only allocations';

        my $new_zaaktype = $zs->create_zaaktype_predefined_ok;

        my $new_zaaktype_statuses = $new_zaaktype->zaaktype_node_id->zaaktype_statuses;

        my $sjabloon = $zs->create_zaaktype_sjabloon_ok(
            status => $new_zaaktype_statuses->find({ status => 2 })
        );

        lives_ok {
            $zaak->wijzig_zaaktype({ zaaktype_id => $new_zaaktype->id })
        } 'wijzig_zaaktype lives';

        # Manually initialize, normally a controller would do this implicitly.
        lives_ok {
            $zaak->case_actions_cine;
        } 'reinitialize case actions';

        my $action = $zs->schema->resultset('CaseAction')->find({
            case_id => $zaak->id,
            type => 'template'
        });

        isa_ok $action, 'Zaaksysteem::Backend::Case::Action::Component',
            'template case action exists';

        is $action->data->{ bibliotheek_sjablonen_id }, $sjabloon->bibliotheek_sjablonen_id->id,
            'template case action references correct library template';
    });
}

sub zs_zaken_suspension_rationale : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $zaak = $zs->create_case_ok;
        my $reason = Zaaksysteem::TestUtils::generate_random_string();

        my $event = $zaak->wijzig_status({
            status => 'stalled',
            reason => $reason
        });

        isa_ok $event, 'Zaaksysteem::DB::Component::Logging';

        is $reason, $event->data->{ reason }, 'Reason for case suspension logged in event';

        my $object = $zaak->object_data;

        my $attr = $object->get_object_attribute('case.suspension_rationale');

        is $reason, $attr->value, 'Case object_data row has reason attribute and value';
    });
}

sub zs_zaken_premature_completion_rationale : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $zaak = $zs->create_case_ok;
        my $reason = Zaaksysteem::TestUtils::generate_random_string();
        my $result = $zaak->zaaktype_node_id->zaaktype_resultaten->first;

        # Emulate controller behavior
        $zaak->resultaat($result->resultaat);
        $zaak->set_gesloten(DateTime->now());

        my $event = $zaak->logging->trigger('case/early_settle', {
            component => 'zaak',
            data => {
                case_id => $zaak->id,
                reason => $reason,
                case_result => $zaak->resultaat
            }
        });

        $zaak->_touch;

        my $object = $zaak->object_data;

        my $attr = $object->get_object_attribute('case.premature_completion_rationale');

        is $reason, $attr->value, 'Case object_data row has premature completion reason attribute and value';
    });
}

sub zs_zaken_regression_zs8229 : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $zaaktype_with_checklist = $zs->create_zaaktype_predefined_ok;
        my $zaaktype_with_checklist_statuses = $zaaktype_with_checklist->zaaktype_node_id->zaaktype_statuses;
        my $zaaktype_without_checklist = $zs->create_zaaktype_predefined_ok;

        my $zaak = $zs->create_case_ok;

        my $phase_count = $zaak->zaaktype_node_id->zaaktype_statuses->count;

        my @checklist_items = map { $_->checklist_items->all } $zaak->checklists->all;

        is scalar @checklist_items, 0, 'default case has no checklist items';

        my $checklist_item = $zs->create_zaaktype_status_checklist_item_ok(
            status => $zaaktype_with_checklist_statuses->find({ status => 2 })
        );

        lives_ok {
            $zaak->wijzig_zaaktype({ zaaktype_id => $zaaktype_with_checklist->id })
        } 'wijzig_zaaktype lives';

        my @items = map { $_->checklist_items->all } $zaak->checklists->all;

        is scalar @items, 1, 'changed casetype has checklist item';

        is $items[0]->label, $checklist_item->label, 'correct checklist item';

        lives_ok {
            $zaak->wijzig_zaaktype({ zaaktype_id => $zaaktype_without_checklist->id })
        } 'wijzig_zaaktype lives';

        @items = map { $_->checklist_items->all } $zaak->checklists->all;

        is scalar @items, 0, 'changed casetype has no checklist items';
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

