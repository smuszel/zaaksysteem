package TestFor::General::Tools;

# ./zs_prove -v t/lib/TestFor/General/Tools.pm
use base qw(ZSTest);

use TestSetup;

use FakeLogger;
use BTTW::Tools qw[
    define_profile
    throw
    burp
    barf
    sanitize_filename
    assert_date
];

use Test::DummySignature;
use Test::DummyProfile;

sub import_tests : Tests {
    lives_ok(
        sub { define_profile("test" => ()); },
        "define_profile() was imported",
    );

    throws_ok(
        sub { throw("aap", "noot") },
        "Zaaksysteem::Exception::Base",
        "throw() was imported"
    );
}

sub basic_wrapped_profile : Tests {
    my $obj = Test::DummyProfile->new;

    my ($obj_self, $opts, @rest) = $obj->basic_wrapped_args(foo => 'bar');

    is $obj_self, $obj, 'first argument is $self';
    is ref $opts, 'HASH', 'second argument is opts hash';
    is $opts->{ foo }, 'bar', 'opts hash contains provided key/value';
    is scalar keys %{ $opts }, 1, 'opts hash contains only provided key/value';
    is scalar @rest, 0, 'no extra arguments';
}

sub custom_wrapped_profile : Tests {
    my $obj = Test::DummyProfile->new;

    my @args = (
        'first',
        { data => { bar => 2 } },
        'third'
    );

    my ($obj_self, $opts, @rest) = $obj->custom_wrapped_args(@args);

    is $obj_self, $obj, 'first argument is $self';
    is ref $opts, 'HASH', 'second argument is opts hash';
    is $opts->{ bar }, 2, 'opts hash contains provided key/value';
    is scalar keys %{ $opts }, 1, 'opts hash contains only provided key/value';
    is scalar @rest, 3, '3 extra arguments';
    is_deeply \@rest, \@args, 'original argumentlist preserved';
}

sub generate_message_tests : Tests {
    my $ref_msg = BTTW::Tools::_generate_message({ a => "reference" });
    is($ref_msg, qq[{ a => "reference" }$/], "Reference is dumped properly");

    my $filtered_msg = BTTW::Tools::_generate_message({ schema => $zs->schema });
    is(
        $filtered_msg,
        sprintf(
            qq[{\n  schema => <Zaaksysteem::Schema database handle connected to %s>,\n}$/],
            $zs->schema->storage->connect_info->[0]{dsn}
        ),
        "Database object was properly flattened"
    );

    my $simple_msg = BTTW::Tools::_generate_message("foo");
    is($simple_msg, "foo$/", "Simple string is returned as-is");

    my $sprintf_msg = BTTW::Tools::_generate_message("foo %d", 42);
    is($sprintf_msg, "foo 42$/", "Multiple arguments are passed through sprintf");
}

sub log_tests : Tests {
    my $log = BTTW::Tools::_log("a", "b", "c");
    is($log, "a$/b$/c", "In non-void context, the to-be-logged data is returned");

    with_stopped_clock {
        my $warnings = "";
        local $SIG{__WARN__} = sub { $warnings .= join("", @_) };

        BTTW::Tools::_log("a", "b", "c");
        is(
            $warnings,
            localtime . " a$/b$/c$/",
            "In void context with no logger object, data is warned.",
        );
    };

    my $logger = FakeLogger->new();
    BTTW::Tools::set_logger($logger);
    BTTW::Tools::_log("a", "b", "c");

    is_deeply(
        $logger->log->{debug},
        ["a$/b$/c"],
        "Log object is set, and _log is called in void context",
    );
}

sub toplevel_tests : Tests {
    my $msg_burp = burp("Ooh, %s-flavoured!", "lemon");
    is($msg_burp, "Ooh, lemon-flavoured!\n", "burp() works");

    my $msg_barf = barf("Ooh, %s-flavoured!", "pistachio");
    like($msg_barf, qr/^Ooh, pistachio-flavoured!$/m, "barf() output contains the message");
    like($msg_barf, qr/^Trace begun at/m, "barf() output contains a stack trace");
}

sub sanitize_filename_tests : Tests {
    {
        my $evil_filename = "C:\\Program Files\\<b>To boldly go where no man has gone before</b>.odt";
        is(sanitize_filename($evil_filename), "b_.odt", 'Filename containing all kinds of bad stuff');
    }

    {
        my $evil_filename = qq{<>"&};
        is(sanitize_filename($evil_filename), "____", "Filename composed of all 'bad' characters'");
    }

    {
        my $evil_filename = qq{C:\\Windows\\System32\\TEST.exe};
        is(sanitize_filename($evil_filename), "TEST.exe", "Normal Windows/MSIE full-path filename");
    }

    {
        my $evil_filename = qq{VerySafe.odt};
        is(sanitize_filename($evil_filename), "VerySafe.odt", "Normal filename");
    }
}

sub test_elfproef : Tests {

    BTTW::Tools->import('elfproef');

    ok(elfproef('123456789'), "123456789 is elfproef");
    ok(!elfproef('552297610'), "552297610 is not elfproef");

    my $bsn = Zaaksysteem::TestUtils->generate_bsn();
    ok(elfproef($bsn), "BSN $bsn is elfproef");
}

sub assert_date_tests : Tests {

    my @ok = ('15-04-2015', DateTime->now());

    foreach(@ok) {
        isa_ok(assert_date($_), 'DateTime', "$_ is a date");
    }

    throws_ok(
        sub {
            assert_date("65-04-2014");
        },
        qr/assert_date: Incorrect datumformaat/,
    );

}

sub signature_tests : Tests {
    my $obj = Test::DummySignature->new;

    lives_ok {
        is $obj->test_simple(1), 1, 'retval checks out'
    } 'simple positive test signature is ok';

    dies_ok {
        $obj->test_simple('Str')
    } 'simple negative test signature is ok';

    throws_ok { $obj->test_simple('Str') } qr[sig\/test_simple\/check_arguments];
    throws_ok { $obj->test_simple('Str') } 'Zaaksysteem::Exception::Base';

    lives_ok {
        my $ok = $obj->test_noargs
    } 'noargs positive test signature ok';

    lives_ok {
        my $ok = $obj->test_noargs(1, 'str', {})
    } 'noargs positive test is lenient about additional args';

    lives_ok {
        my $ok = $obj->test_noret('')
    } 'noret positive test ok';


    lives_ok {
        is $obj->test_slurp_array(20, qw[a b c]), 3, 'filled list retval'
    } 'slurp_array positive test ok';

    lives_ok {
        is $obj->test_slurp_array(20), 0, 'empty list retval'
    } 'slurp_array positive, no elements, test ok';

    dies_ok {
        $obj->test_slurp_array(20, {}, [])
    } 'slurp_array negative, wrong type ok';

    lives_ok {
        $obj->test_slurp_typed_array({ foo => 'bar' }, { baz => 'quux' })
    } 'slurp_typed_array positive with hashref args';

    dies_ok {
        $obj->test_slurp_typed_array({ foo => \1 }, { bar => \2 })
    } 'slurp_typed_array negative with hashref args';

    lives_ok {
        my $ok = $obj->test_slurp_hash(20, a => 1, b => 2);
    } 'slurp_hash positive with defined vals';

    dies_ok {
        $obj->test_slurp_typed_hash(a => \1, b => \2);
    } 'slurp_typed_hash negative with nested type mismatch';

    $obj->fail(1);

    throws_ok { $obj->test_simple(0) } qr[sig\/test_simple\/check_return_value];
    throws_ok { $obj->test_simple(0) } 'Zaaksysteem::Exception::Base';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
