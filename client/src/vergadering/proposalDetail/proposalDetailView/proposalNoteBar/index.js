import angular from 'angular';
import template from './template.html';
import zsModalModule from '../../../../shared/ui/zsModal';
import templateNoteEdit from './template-noteedit.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import snackbarServiceModule from '../../../../shared/ui/zsSnackbar/snackbarService';
import zsConfirmModule from '../../../../shared/ui/zsConfirm';
import find from 'lodash/find';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalNoteBar', [
			zsModalModule,
			composedReducerModule,
			snackbarServiceModule,
			zsConfirmModule
		])
		.directive('proposalNoteBar', [ '$animate', '$window', '$timeout', '$compile', 'zsModal', 'composedReducer', 'zsConfirm', 'snackbarService', ( $animate, $window, $timeout, $compile, zsModal, composedReducer, zsConfirm, snackbarService ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					proposalReference: '&',
					proposalTitle: '&',
					notes: '&',
					onHandleNote: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element) {

					let ctrl = this,
						noteEditModal,
						noteReducer,
						titleReducer,
						currentNote,
						loading = false;

					titleReducer = composedReducer({ scope: $scope }, ctrl.proposalTitle)
						.reduce( title => title);

					noteReducer = composedReducer({ scope: $scope }, ctrl.notes )
						.reduce( notes => {

							let newNote = [
									{
										reference: '0',
										instance: {
											content: ''
										},
										isEmpty: true
									}
								];

							return notes ?
								notes.concat(newNote)
								: newNote;
						});

					ctrl.getNotes = noteReducer.data;

					ctrl.getTitle = titleReducer.data;

					ctrl.handleNoteClick = ( noteReference ) => {

						let noteModalTemplate = angular.element(templateNoteEdit);

						currentNote = noteReference;

						ctrl.noteContent = find(noteReducer.data(), (item) => item.reference === noteReference).instance.content;

						noteEditModal = zsModal({
							el: $compile(noteModalTemplate)($scope),
							title: ctrl.proposalTitle(),
							classes: 'note-modal'
						});

						noteEditModal.open();

						$timeout(( ) => {
							noteModalTemplate.find('textarea')[0].focus();
						}, 0, false);

					};

					ctrl.saveNote = ( ) => {

						// onHandleNote is used for saving, updating and deleting notes,
						// dependent on which variables are passed.

						loading = true;

						ctrl.onHandleNote({
							$proposalReference: ctrl.proposalReference(),
							$noteReference: currentNote !== '0' ? currentNote : undefined,
							$noteContent: ctrl.noteContent
						}).then( ( ) => {

							$timeout( ( ) => {
								noteEditModal.close();
							}, 0);

							loading = false;

						}).catch( ( ) => {

							snackbarService.error('Er ging iets mis bij het opslaan van uw notitie. Neem contact op met uw beheerder voor meer informatie.');

							loading = false;

						});
					};

					ctrl.deleteNote = ( ) => {

						zsConfirm( 'Weet u zeker dat u deze notitie wilt verwijderen?', 'Ja')
						.then( ( ) => {

							loading = true;

							let element = angular.element($element[0].querySelector(`[data-reference="${currentNote}"]`));

							noteEditModal.close();

							$animate.addClass(element, 'removing');

							ctrl.onHandleNote({
								$proposalReference: ctrl.proposalReference(),
								$noteReference: currentNote
							}).then( ( ) => {

								loading = false;

							}).catch( ( ) => {

								snackbarService.error('Er ging iets mis bij het verwijderen van uw notitie. Neem contact op met uw beheerder voor meer informatie.');

								noteEditModal.close();

								loading = false;

							});

						});
					};

					ctrl.goBack = ( ) => {
						noteEditModal.close();
					};

					ctrl.isLoading = ( ) => loading;

					ctrl.isNew = ( ) => currentNote === '0';

					$scope.$on('$destroy', ( ) => {
						if (noteEditModal) {
							noteEditModal.close();
						}
					});


				}],
				controllerAs: 'proposalNoteBar'

			};
		}
		])
		.name;
