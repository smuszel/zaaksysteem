import angular from 'angular';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsReactIframe', [])
		.directive(
			'zsReactIframe',
			[() => {
				return {
					restrict: 'E',
					template,
					scope: {
						iframeSrc: '&',
						onLocationChange: '&'
					},
					bindToController: true,
					controller: [ '$scope', function (scope) {
						let ctrl = this;
						
						ctrl.onMessage = (event) => {
							if (event.data && event.data.type === 'locationChange') {
								let iframeUrl = event.data.data;
								if (ctrl.onLocationChange) {
									ctrl.onLocationChange({data: iframeUrl});
								}
							}
						};

						window.addEventListener('message', ctrl.onMessage);
						scope.$on('$destroy',() => window.removeEventListener('message', ctrl.onMessage));

					}],
					controllerAs: 'vm'
				};

			}])
		.name;
