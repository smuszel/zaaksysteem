import angular from 'angular';
import zsKeyboardNavigableList from '../zsKeyboardNavigableList';
import template from './template.html';
import controller from './controller';

export default angular
	.module('shared.ui.zsSuggestionList', [
		zsKeyboardNavigableList
	])
	.component('zsSuggestionList', {
		bindings: {
			keyInputDelegate: '&',
			suggestions: '&',
			onSelect: '&',
			label: '@'
		},
		controller,
		template
	})
	.name;
