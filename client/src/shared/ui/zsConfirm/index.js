import angular from 'angular';
import zsModalModule from './../zsModal';
import template from './template.html';

export default
	angular.module('zsConfirm', [
		zsModalModule
	])
		.factory('zsConfirm', [ '$interpolate', '$rootScope', '$compile', '$q', 'zsModal', ( $interpolate, $rootScope, $compile, $q, zsModal ) => {

			let tpl = template;

			/* here be dragons */
			if ($interpolate.startSymbol() !== '{{') {
				tpl = tpl.replace(/{{/g, $interpolate.startSymbol())
					.replace(/}}/g, $interpolate.endSymbol());
			}

			return ( label, verb = 'Bevestigen', options ) => {

				return $q( ( resolve, reject ) => {

					let scope = $rootScope.$new(true);

					let modal = zsModal({
						title: 'Bevestigen',
						el: $compile(tpl)(scope),
						classes: 'confirm'
					});

					let cleanup = ( ) => {
						scope.$destroy();
					};


					scope.vm = {
						label,
						verb,
						options,
						values : {},
						onConfirm: ( ) => {
							modal.close();
							cleanup();
							resolve(scope.vm.values);
						},
						onCancel: ( ) => {
							modal.close();
							cleanup();
							reject();
						}
					};

					modal.onClose(( ) => {
						cleanup();
						reject();
					});

					modal.open();

				});

			};

		}])
		.name;
