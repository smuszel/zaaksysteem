import get from 'lodash/get';
import getAttributeTypeEligibility from './getAttributeTypeEligibility';

const getEligibleForV2 = (casetypeV2, recipient, fields, vals) => {
  // a casetype property that is false when the casetype is configured with:
  // - use address of requestor for correspondence
  // - use address of requester as case address
  // - any rules in the registration phase
  const casetypeEligibility = get(casetypeV2, 'meta.is_eligible_for_case_creation');

  // additional subjects can be related manually
  // v2 does not yet support this
  const subjectsEligibility = vals.$related_subjects === undefined || vals.$related_subjects.length === 0;

  // the following attribute types are not yet supported by v2:
  // - appointment
  // - subject (Betrokkene)
  // - document
  // - addresses (6x BAG & googleMaps)
  const attributeTypeEligibility = getAttributeTypeEligibility(fields, vals);

  // a case registration initiated from the documentintake will include separate files
  // v2 does not yet support this
  const documentintakeEligibility = vals.$files === undefined;

  // a case can have a recipient
  // v2 does not yet support this
  const recipientEligibility = recipient === undefined;

  const eligibilities = [casetypeEligibility, subjectsEligibility, attributeTypeEligibility, documentintakeEligibility, recipientEligibility];

  return eligibilities.every(Boolean);
};

export default getEligibleForV2;
