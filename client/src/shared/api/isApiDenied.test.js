import isApiDenied, {
	CONFIGURATION_INCOMPLETE,
	isIncomplete,
	isUnauthorized
} from './isApiDenied';


const dataIncomplete = {
	result: {
		instance: {
			type: CONFIGURATION_INCOMPLETE
		}
	}
};

describe('The `isApiDenied` module', () => {
	describe('exports a default function that', () => {
		test('returns `true` if the status is 401', () => {
			const reason = {
				status: 401,
				config: {
					url: '/our/api'
				}
			};

			expect(isApiDenied(reason)).toBe(true);
		});

		test('returns `true` if the URL has the same origin and the configuration is incomplete', () => {
			const reason = {
				status: 200,
				config: {
					url: '/our/api'
				},
				data: dataIncomplete
			};

			expect(isApiDenied(reason)).toBe(true);
		});

		test(
			'returns `false` if the URL has another origin and happens to have a response object that resembles ours',
			() => {
				const reason = {
					status: 200,
					config: {
						url: '//their/api'
					},
					data: dataIncomplete
				};

				expect(isApiDenied(reason)).toBe(false);
			});

		test('returns `false` if the URL has another origin', () => {
			const reason = {
				status: 401,
				config: {
					url: '//their/api'
				}
			};

			expect(isApiDenied(reason)).toBe(false);
		});
	});

	describe('exports a utility function that', () => {
		test('returns `false` if the URL has the same origin and type is not incomplete', () => {
			expect(isIncomplete('NIL', '/my/api')).toBe(false);
		});

		test('returns `true` if the URL has the same origin and type is incomplete', () => {
			expect(isIncomplete(CONFIGURATION_INCOMPLETE, '/my/api')).toBe(true);
		});

		test('returns `false` if the URL has another origin and type is not incomplete', () => {
			expect(isIncomplete('NIL', '//their/api')).toBe(false);
		});

		test('returns `false` if the URL has another origin and type is incomplete', () => {
			expect(isIncomplete(CONFIGURATION_INCOMPLETE, '//their/api')).toBe(false);
		});
	});

	describe('exports a utility function that', () => {
		test('returns `true` if the URL has the same origin and the status is 401', () => {
			expect(isUnauthorized(401, '/my/api')).toBe(true);
		});

		test('returns `false` if the URL has the same origin and the status is not 401', () => {
			expect(isUnauthorized(200, '/my/api')).toBe(false);
		});

		test('returns `false` if the URL has another origin and the status is 401', () => {
			expect(isUnauthorized(401, '//their/api')).toBe(false);
		});
	});
});
