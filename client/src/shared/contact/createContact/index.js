import angular from 'angular';
import composedReducerModule from '../../api/resource/composedReducer';
import inputModule from './../../vorm/types/input';
import radioModule from './../../vorm/types/radio';
import resourceModule from '../../api/resource';
import selectModule from './../../vorm/types/select';
import sessionServiceModule from '../../user/sessionService';
import vormFieldsetModule from './../../vorm/vormFieldset';
import vormValidatorModule from '../../vorm/util/vormValidator';
import controller from './CreateContactController';
import template from './template.html';

export default angular
  .module('createContact', [
    composedReducerModule,
    inputModule,
    radioModule,
    resourceModule,
    selectModule,
    sessionServiceModule,
    vormFieldsetModule,
    vormValidatorModule
  ])
  .component('createContact', {
    bindings: {
      isPristine: '&',
      onClose: '&'
    },
    controller,
    template
  })
  .name;
