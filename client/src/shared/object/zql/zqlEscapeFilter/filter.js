export default ( input ) =>
	typeof input !== 'string' ?
	input
	: `"${input.replace(/(["\\])/g, '\\$1').replace(/;/g, '%3B')}"`;
