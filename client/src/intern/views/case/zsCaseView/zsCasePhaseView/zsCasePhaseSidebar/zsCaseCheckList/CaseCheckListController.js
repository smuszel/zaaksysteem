import shortid from 'shortid';
import orderBy from 'lodash/orderBy';

export default class CaseCheckListController {

	static get $inject() {
		return ['$scope', 'composedReducer'];
	}

	constructor( $scope, composedReducer ) {
		let ctrl = this;

		ctrl.newItemContent = '';

		ctrl.getItems = composedReducer({ scope: $scope }, ctrl.items, ctrl.disabled)
			.reduce(( items, isDisabled ) => {
				const orderedItems = orderBy(items, 'id', 'asc');

				return (orderedItems || []).map(item => ({
					$id: shortid(),
					id: item.id,
					label: item.label,
					checked: item.checked,
					togglable: !isDisabled && true,
					deletable: !isDisabled && !!item.user_defined,
					item,
					classes: {
						disabled: isDisabled,
						checked: item.checked
					}
				}));
			})
			.data;

		ctrl.isLoading = () => ctrl.checklistLoading();

		ctrl.handleItemToggle = ( item ) => {
			ctrl.onItemToggle({
				$item: item.item,
				$checked: !item.checked
			});
		};

		ctrl.handleItemRemove = ( item ) => {
			ctrl.onItemRemove({ $item: item.item });
		};

		ctrl.handleItemAdd = ( content ) => {
			ctrl.onItemAdd({ $content: content });
			ctrl.newItemContent = '';
		};
	}

}
