import { extract } from '../../api/contact';

export default class ContactInformationController {
	constructor( sce ) {
		this.fields = extract(this.contact, sce);
	}

	get contact() {
		return this.subject().data();
	}

	get contactInformation() {
		return this.fields;
	}

	get isMutable() {
		return false;
		// ZS-TODO: implement editing internal contacts (depends on API endpoint)
		//          when `this.contact.instance.external_subscription` is not empty
	}
}
