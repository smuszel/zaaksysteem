export default [
    {
        name: 'Testbedrijf',
        id: '1',
        uuid: 'a734a83f-77d5-40e5-b0e6-5462f47cb54c'
    },
    {
        name: 'Tulsi Trekking',
        id: '7',
        uuid: 'f135b4ac-4d78-4cc9-99e4-b00316c268f5'
    },
    {
        name: 'Contactgegevens aanpassen leeg',
        id: '8',
        uuid: 'f3c4d757-e0c6-4d5d-8f21-958565cf9420'
    },
    {
        name: 'Contactgegevens aanpassen veranderen',
        id: '9',
        uuid: 'b808a557-9aa7-49fa-8cd4-0f48bf372c08'
    },
    {
        name: 'Contactgegevens aanpassen verwijderen',
        id: '10',
        uuid: '8df7d17c-3995-44b9-97c3-762d3d4af447'
    }
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
