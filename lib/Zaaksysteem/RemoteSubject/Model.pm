package Zaaksysteem::RemoteSubject::Model;
use Moose;

=head1 NAME

Zaaksysteem::RemoteSubject::Model - An Zaaksysteem/company API bridge model

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::RemoteSubject::Model;
    my $model = Zaaksysteem::RemoteSubject::Model->new(
        schema => $schema
    );

=cut

use Zaaksysteem::BR::Subject;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object

=cut

has schema => (
    is       => 'ro',
    isa      => "Zaaksysteem::Schema",
    required => 1,
);

=head2 _interface

Cache/storage location for the L<Zaaksysteem::Schema::Interface> instance
returned by L<#get_interface>.

=cut

has _interface => (
    is => 'rw',
    isa => 'Zaaksysteem::Schema::Interface',
);

=head2 _bridge

Cache/storage location for the L<Zaaksysteem::BR::Subject> instance returned by
L<#get_bridge>.

=cut

has _bridge => (
    is => 'rw',
    isa => 'Zaaksysteem::BR::Subject',
);

=head2 get_interface

Get the KvK API or OverheidIO interface

=cut

sub get_interface {
    my $self = shift;

    return $self->_interface if $self->_interface;

    my (@interfaces) = $self->schema->resultset("Interface")->search_active(
        {module => ["kvkapi", "overheidio"]}
    )->all;

    if (@interfaces > 1) {
        throw(
            "remote_subject/multiple_interfaces",
            sprintf(
                "More than one active KvK API or Overheid IO interface was found (%d > 1).",
                scalar @interfaces,
            ),
        )
    }

    if (@interfaces) {
        $self->_interface($interfaces[0]);
        return $self->_interface;
    }

    $self->log->info("No active KvK API or OverheidIO interface");
    return;
}

=head2 get_bridge

Get the L<Zaaksysteem::BR::Subject> object.

Returns nothing if there is no eHerkenning/OverheidIO or eHerkenning/KvK-API link.

=cut

sub get_bridge {
    my $self = shift;

    return unless $self->has_eherkenning;
    return $self->_bridge if $self->_bridge;

    # There must be an interface if ->has_eherkenning is true.
    my $interface = $self->get_interface;

    my $bridge = Zaaksysteem::BR::Subject->new(
        schema        => $self->schema,
        remote_search => $interface->module,
    );

    $self->_bridge($bridge);

    return $bridge;
}

=head2 has_eherkenning

Check if you can query OverheidIO or the KvK API from eHerkenning requests.

=cut

sub has_eherkenning {
    my $self = shift;

    if (my $interface = $self->get_interface) {
        my $config = $interface->get_interface_config;
        
        return 1 if $config->{overheid_io_eherkenning_request};
        return 1 if $config->{kvkapi_eherkenning_request};
    }

    return 0;
}

=head2 get_company_from_remote

Search for a company at OverheidIO or using the KvK API

=cut

sub get_company_from_remote {
    my $self = shift;
    my %opts = @_;

    my $br = $self->get_bridge;
    return unless $br;

    my @objects = $br->search(
        {
            subject_type         => 'company',
            'subject.coc_number' => $opts{kvknummer},
            $opts{vestigingsnummer}
            ? ('subject.coc_location_number' => int($opts{vestigingsnummer}))
            : (),
        }
    );
    if (!@objects) {
        throw("remote_subject/company_not_found", "Unable to find company");
    }

    if (@objects == 1) {
        $br->remote_import($objects[0]);
        return \@objects;
    }
    else {
        return \@objects;
    }
}

=head2 update_company_from_remote

Updates the betrokkene with information from OverheidIO or the KvK API.

Only updates the betrokkene if:

=over

=item * C<vestigingsnummer_override_used> has not been set in the session

=item * A KvK API or OverheidIO interface is active

=back

=cut

sub update_company_from_remote {
    my ($self, $session, $betrokkene) = @_;

    if (!$betrokkene) {
        $self->log->debug("No betrokkene, not updating from remote");
        return 0;
    }

    if ($session->{vestigingsnummer_override_used}) {
        $self->log->debug("Override used, not updating from remote");
        return 0;
    }

    my $br = $self->get_bridge;
    return undef unless $br;

    my $b = $betrokkene->gm_bedrijf;
    my @objects = $br->search(
        {
            subject_type                  => 'company',
            'subject.coc_number'          => $b->dossiernummer,
            'subject.coc_location_number' => $b->vestigingsnummer,
        }
    );

    if (@objects == 1) {
        $br->remote_import($objects[0]);
    }
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
