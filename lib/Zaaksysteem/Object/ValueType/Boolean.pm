package Zaaksysteem::Object::ValueType::Boolean;

use Moose;
use namespace::autoclean;

with 'Zaaksysteem::Interface::ValueType';

=head1 NAME

Zaaksysteem::Object::ValueType::Boolean - L<Zaaksysteem::Object> value type
for boolean values

=head1 DESCRIPTION

This class abstracts ZSS C<boolean> values.

=cut

use BTTW::Tools;

=head1 METHODS

=head2 name

Implements type name constant interface for
L<Zaaksysteem::Interface::ValueType>.

Returns the static string C<boolean>.

=cut

sub name {
    return 'boolean';
}

=head2 equal

Implements equality testing of two values in C<boolean> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->as_bool(shift);
    my $b = $self->as_bool(shift);

    return $a && $b;
}

=head2 not_equal

Implements non-equality testing of two values in C<boolean> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 as_bool

Returns a boolean-ish perl scalar derived from the provided
L<Zaaksysteem::Object::Value>-implementing value.

B<Note>: this method assumes all defined values are true-ish, and all
undefined values are false-ish. This means the perl semantics of empty string
C<''>, C<int(0)>, and the empty list C<()> do not apply for ZSS values.

=cut

sub as_bool {
    my $self = shift;
    my $value = shift;

    return $value->value if $value->type_name eq 'boolean';
    return $value->has_value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
