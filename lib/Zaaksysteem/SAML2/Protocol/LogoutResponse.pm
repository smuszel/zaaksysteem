package Zaaksysteem::SAML2::Protocol::LogoutResponse;
use Moose;

extends 'Net::SAML2::Protocol::LogoutResponse';
with 'Zaaksysteem::SAML2::Protocol::Role::RandomID';

use MooseX::Types::Moose qw/ Str /;
use XML::Generator;

=head1 NAME

Zaaksysteem::SAML2::Protocol::LogoutResponse

=head1 DESCRIPTION

This module wraps L<Net::SAML2::Protocol::LogoutRequest> and outputs XML that
AD FS can understand.

=head1 Example usage

Constructor is called in default L<Moose> style.

    my $logoutres = Zaaksysteem::SAML2::Protocol::LogoutResponse->new(
        issuer      => $issuer,
        destination => $destination,
        status      => $status,
        response_to => $response_to,
    );

    my $str = $logoutres->as_xml;

=head1 Attributes

=head1 Inherited attributes

See L<Net::SAML2::Protocol::LogoutResponse>

=cut

=head1 Methods

=head2 as_xml

Returns a L<XML::Generator::final> object representing the
C<LogoutResponse> XML message.

=cut

sub as_xml {
    my ($self) = @_;

    my $x = XML::Generator->new(':pretty');
    my $saml  = ['saml' => 'urn:oasis:names:tc:SAML:2.0:assertion'];
    my $samlp = ['samlp' => 'urn:oasis:names:tc:SAML:2.0:protocol'];

    $x->xml(
        $x->LogoutResponse(
            $samlp,
            { ID => $self->random_id,
              Version => '2.0',
              IssueInstant => $self->issue_instant,
              Destination => $self->destination,
              InResponseTo => $self->response_to },
            $x->Issuer(
                $saml,
                $self->issuer,
            ),
            $x->Status(
                $samlp,
                $x->StatusCode(
                    $samlp,
                    { Value => $self->status },
                )
            )
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
