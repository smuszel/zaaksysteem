package Zaaksysteem::Model::Geo;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

extends 'Catalyst::Model::Factory';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Geo::Model',
    constructor => 'new',
);

=head1 NAME

Zaaksysteem::Model::Geo - Catalyst model factory for
L<Zaaksysteem::Geo::Model> instances.

=head1 DESCRIPTION

Implements a L<Catalyst::Model::Factory> for L<Zaaksysteem::Geo::Model>.

=head1 SYNOPSIS

    my $model = $c->model('Geo');

=head1 METHODS

=head2 prepare_arguments

Implements argument preparation for model instantiation. This preparation will
search Zaaksysteem for configured Geocoder integrations.

=head3 Integration support

=over 4

=item L<Zaaksysteem::Backend::Sysin::Modules::OpenCage>

L<OpenCage|http://www.opencage.com> is a provider of (reverse) geocoding
services.

=back

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    my @modules = qw[opencage];

    my $if_rs = $c->model('DB::Interface')->search_active({
        module => \@modules
    });

    my $sysin_config = $c->config->{ Sysin };

    my %opts =  map { $_ => $sysin_config->{ $_ } }
               grep { exists $sysin_config->{ $_ } }
                    @modules;

    return {
        interfaces => [ $if_rs->all ],
        module_opts => \%opts
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
