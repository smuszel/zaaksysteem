package Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI;
use Moose::Role;
use URI;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI

=cut

with 'MooseX::Log::Log4perl';

has uri_values => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    # Modify the base_url for Multi Tenant enabled interfaces
    if ($self->does('Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant') && $self->schema->is_multi_tenant) {
        my $interface = $opts->{entry};
        my $config = $interface->get_interface_config;
        if ($config->{is_multi_tenant}) {
            $opts->{base_url} = sprintf("https://%s/", $config->{multi_tenant_host});
        }
    }

    my %values;
    for my $param (keys %{ $self->uri_values }) {
        $values{$param} = $self->new_uri($opts->{base_url}, $self->uri_values->{$param});
    }

    if ($self->can('services_uri_values')) {
        for my $param (keys %{ $self->services_uri_values }) {
            $values{$param} = $self->new_uri($opts->{services_url}, $self->services_uri_values->{$param});
        }
    }

    $form->load_values(\%values);

    return $form;
};

=head2 new_uri

Creates a new URI as a string from a base and a path.

    my $url = $self->new_uri('http://foo', 'path');

=cut

sub new_uri {
    my ($self, $base_uri, $path) = @_;
    return URI->new_abs($path, $base_uri)->as_string;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
