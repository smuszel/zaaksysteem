package Zaaksysteem::Backend::Sysin::Modules::ValidSign;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with 'Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::ValidSign - ValidSign integration module

=head1 DESCRIPTION

This sysin module declares the Zaaksysteem interface for interactions with
the L<www.validsign.nl|http://www.validsign.nl> REST API.

=cut

use BTTW::Tools;
use BTTW::Tools::UA qw(new_user_agent);
use Zaaksysteem::External::ValidSign;
use Zaaksysteem::ZAPI::Error;

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Collection of L<Zaaksysteem::ZAPI::Form> objects used in building the UI for
this module.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_endpoint',
        type => 'text',
        label => 'ValidSign API Endpoint',
        description => 'API Endpoint URL van de ValidSign service',
        required => 1,
        data => { placeholder => 'https://try.validsign.nl/api' }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_key',
        type => 'text',
        label => 'ValidSign API Key',
        description => 'API Key van de ValidSign service, dit gegeven kan gevonden worden op de "Account Information" pagina van het gebruikersprofiel in de ValidSign interface',
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name  => 'interface_api_default_sender',
        type  => 'text',
        label => 'Standaardgebruiker',
        description =>
            'Standaardgebruiker indien de Zaaksysteemgebruiker geen ValidSign-account heeft',
        data => { placeholder => 'user@validsign.nl' }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_callback_endpoint',
        type => 'display',
        label => 'ValidSign Callback Endpoint',
        description => 'API Endpoint URL waarmee ValidSign ondertekende document in Zaaksysteem verwerkt',
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    ),
];

=head2 INTERFACE_DESCRIPTION

Module description / pointers for the UI.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
    <a href="https://www.validsign.nl/" hreflang="nl-NL" target="_blank">
    ValidSign</a> is een service waarmee documenten digitaal ondertekend
    kunnen worden.
</p>

<p>
    Dit koppelprofiel configureert Zaaksysteem zodat gebruikers in de
    documententab van een zaak een document kunnen aanbieden om ondertekend te
    worden. Vervolgens zal de gebruiker een e-mail ontvangen met een link naar
    de ValidSign applicatie om dit proces te voltooien.
</p>

<p>
    Na een succesvolle ondertekening van een document wordt het door ValidSign
    teruggestuurd naar Zaaksysteem, waar het als nieuw en ongeaccepteer
    document teruggevonden kan worden door de gebruiker.
</p>
EOD

=head2 INTERFACE_TRIGGERS

Set of triggers available in the module.

=cut

use constant INTERFACE_TRIGGERS => [
    { method => 'receive_event', update => 0, public => 1 },
    { method => 'start_signing_procedure' },
];

=head2 MODULE_SETTINGS

Sysin module parameters, used during instantiation to configure the module.

=cut

use constant MODULE_SETTINGS => {
    name                          => 'valid_sign',
    label                         => 'ValidSign Electronische Handtekening',
    description                   => INTERFACE_DESCRIPTION,
    interface_config              => INTERFACE_CONFIG_FIELDS,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    trigger_definition            => {
        map { $_->{method} => $_ } @{ INTERFACE_TRIGGERS() }
    }
};

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class) = @_;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 receive_event

    $module->receive_event({ parameters => 'value' }, $interface);

Receive event trigger of the interface, processes the JSON send to the
interface endpoint

=cut

sub receive_event {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_process_receive_event',
                %$params,
            },
            direction               => 'incoming',
            external_transaction_id => $params->{packageId} // 'unknown',
            input_data =>
                JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    if ($transaction->error_count) {
        return Zaaksysteem::ZAPI::Error->new(
            type => 'general/error',
            messages => [
                "Error in transaction: " . $transaction->id,
                $transaction->preview_data->[0]{preview_string},
            ]
        );
    }
    return [
        {
            transaction_id => $transaction->id,
            message        => $transaction->preview_data->[0]{preview_string},
        }
    ];
}

=head2 start_signing_procedure

    $module->start_signing_procedure(
        {
            case_id       => 42,
            file_id       => 666,
            betrokkene_id => '...',
        },
        $interface
    );

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    my $model = $interface->model;
    $model->receive_event(%$params);

    $record->output(
        sprintf(
            "Received call back from external party:" . dump_terse($params)
        )
    );

    $transaction->preview_data({ preview_string => $record->output });

=cut

sub start_signing_procedure {
    my ($self, $params, $interface) = @_;

    $interface->process(
        {
            processor_params => {
                processor      => '_process_start_signing_procedure',
                %$params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data =>
                JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );
}

=head1 PRIVATE METHODS

=head2 _load_values_into_form_object

Hooks into interface initialization routine to inject default values for
specified config fields.

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # don't clobber @_ further than original args

    my $form = $self->$orig(@_);
    my $interface = $opts->{ entry };

    $form->load_values({
        interface_callback_endpoint => sprintf(
            '%ssysin/interface/%d/trigger/receive_event',
            $opts->{ base_url },
            $interface->id,
        )
    });

    return $form;
};

sub _process_receive_event {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    my $model = $interface->model;

    my $case;
    try {
        my $callback = $model->receive_event($params->{name});
        $case = $callback->($model, %$params);
    }
    catch {
        $self->log->info(
            sprintf(
                "Unable to complete transaction %d: %s",
                $transaction->id, $_
            )
        );
        die $_;
    }
    finally {
        my @msg = ("Aangeleverd package $params->{packageId}");
        if ($transaction->error_count) {
            push(@msg, "niet worden verwerkt")
        }
        else {
            push(@msg, "is verwerkt");
        }

        if ($case) {
            unshift(@msg, "Zaak", $case->id, ":");
        }

        $transaction->preview_data({ preview_string => join(" ", @msg) });
        $record->preview_string(join(" ", @msg));
    };
    return $transaction;
}

sub _process_start_signing_procedure {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    foreach (qw(base_url message processor statusCode statusText kenmerken)) {
        delete $params->{$_} if exists $params->{$_};
    }

    my $model = $interface->model;
    my $pkg   = $model->create_signing_request(%$params,
        interface_id => $interface->id);

    $record->output(
        sprintf(
            "Created external document package: %s with id %s for case %d",
            $pkg->name, $pkg->id, $params->{case_id},
        )
    );

    my $preview_data = sprintf(
        "Zaak %d: Bestand %s opgestuurd voor ondertekening",
        $params->{case_id}, $pkg->documents->[0]->name,
    );

    $transaction->preview_data({ preview_string => $preview_data });
    $record->preview_string($preview_data);
    return $transaction;

}

sub _get_model {
    my ($self, $opts) = @_;

    my $config = $opts->{interface}->get_interface_config;
    my $schema = $opts->{interface}->result_source->schema;

    my $bridge = Zaaksysteem::BR::Subject->new(
        schema => $schema,
    );

    return Zaaksysteem::External::ValidSign->new(
        secret                 => $config->{api_key},
        endpoint               => $config->{api_endpoint},
        subject                => $bridge,
        file_rs                => $schema->resultset('File'),
        object_subscription_rs => $schema->resultset('ObjectSubscription'),
        case_rs                => $schema->resultset('Zaak'),
        interface              => $opts->{interface},

        # This way we can test it on sprint without having to have a legit
        # sender
        defined $config->{api_default_sender}
        ? (default_sender => $config->{api_default_sender})
        : ()
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
