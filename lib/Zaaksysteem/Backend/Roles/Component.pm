package Zaaksysteem::Backend::Roles::Component;

use Moose;

use BTTW::Tools;
use List::Util qw[first];
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_AUTHORIZATION_ROLES];
use Zaaksysteem::Object::Types::Role;
use Zaaksysteem::Types qw(Boolean NonEmptyStr);
use Zaaksysteem::Constants::Rights qw(USER DASHBOARD);

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Backend::Roles::Component - Implement specific behaviors for
Roles rows.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 parent_id

The parent_id of this role.

=cut

has parent_id => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return shift->parent_group_id;
    }
);

=head2 object

Holds a reference to a L<Zaaksysteem::Object::Types::Role> instance, which
encodes this row in the L<Zaaksysteem::Object> style.

=cut

has object => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Types::Role',
    lazy => 1,
    builder => 'as_object'
);

=head2 parent_groups

Holds a list of parent groups for this role

=cut

has parent_groups => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self            = shift;

        my $parent_group    = $self->parent_group_id;

        ### Query all parent groups
        return [ $self->result_source->schema->resultset('Groups')->search(
            {
                id      => { in     => $parent_group->path }
            },
            {
                order_by    => { '-asc' => [\"array_length(me.path, 1)"] },
            }
        ) ];
    }
);

=head2 subjects

Arguments: none

    my $subjects_rs = $groups->subjects;

Will retrieve subjects who have this role.

=cut

has subjects => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        $self->result_source->schema->resultset('Subject')->search(
            {
                $self->id => \'= ANY (role_ids)',
            },
        );
    }
);

=head1 METHODS

=head2 update_role

Argumants: \%params

Return value: $UPDATED_ROW

    $newrole   = $newrole->update_role(
        {
            name            => 'Bestandelaar',
            description     => 'Bestandelaar',
        }
    );

=cut

define_profile 'update_role' => (
    required            => [],
    optional            => {
        name              => NonEmptyStr,
        description       => NonEmptyStr,
        change_systemrole => Boolean,
    }
);

sub update_role {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;

    throw(
        'roles/update_role/cannot_change_system_role',
        'Not authorized to alter systemrole, unless change_systemrole=1'
    ) unless ($params->{change_systemrole} || !$self->system_role);

    for my $key (qw/name description/) {
        $self->$key($params->{$key});
    }

    $self->update;
    return $self->discard_changes();
}

=head2 security_identity

Implements a duck-typed L<Zaaksysteem::Object::SecurityIdentity> for the role
instance. Returns a hashable list C<role, $id>.

=cut

sub security_identity {
    return (role => shift->id);
}

=head2 set_deleted

Arguments: none

    $role->set_deleted;

    ### Not needed:
    # $role->update

Only way to proper delete a role

=cut

sub set_deleted {
    my $self        = shift;

    ### Loop over subjects to remove role_ids
    $self->result_source->schema->txn_do(
        sub {
            my $rs = $self->subjects;
            while (my $subject = $rs->next) {
                my @role_ids = grep({ $_ != $self->id } @{ $subject->role_ids });
                $subject->role_ids(\@role_ids);
                $subject->update;
            }

            $self->role_rights->search_rs()->delete;
            $self->delete;
        }
    );
}

=head2 has_db_right

Check if the role has a right, returns the component if found, undef otherwise.

=cut

sub has_db_right {
    my ($self, $name) = @_;

    # System roles do not have special rights
    return 0 if $self->system_role;

    return $self->role_rights->search_rs(
        { 'rights_name' => $name }
    )->first;
};

=head2 has_db_rights

Check if the role has a combination of rights and returns the resultset if all
the rights are found and returns undef when not found.

=cut

sub has_db_rights {
    my ($self, @names) = @_;

    # System roles do not have special rights
    return 0 if $self->system_role;

    my $rs = $self->role_rights->search_rs(
        { 'rights_name' => { -in => \@names } }
    );

    return $rs if $rs->count == @names;
    return;
}

=head2 assert_custom_role

Assert if a role is a custom and therefore modifiable role

=cut

sub assert_custom_role {
    my $self = shift;
    return unless $self->system_role;
    throw("roles/system/asserted", "Unable, this is a system role!");
}

=head2 is_assignee_role

Check if a role fits the (former) system role of I<Behandelaar>.

=cut

sub is_assignee_role {
    my $self = shift;
    if ($self->system_role && $self->name eq 'Behandelaar') {
        return 1;
    }
    elsif ($self->has_db_rights(USER, DASHBOARD)) {
        return 1;
    }
    return 0;
}

=head2 set_db_right

Set a right on the role

=cut

sub set_db_right {
    my ($self, $name) = @_;

    $self->assert_custom_role;

    return 1 if $self->has_db_rights($name);

    $self->role_rights->create(
        {
            role_id     => $self->id,
            rights_name => $name,
        }
    );
    return 1;

}

=head2 unset_db_right

Unset a right on the role

=cut

sub unset_db_right {
    my ($self, $name) = @_;

    $self->assert_custom_role;

    my $rights = $self->has_db_rights();
    $rights->delete() if $rights;
    return 1;
};

=head2 as_object

Returns the DB object as a Zaaksysteem object

=cut

sub as_object {
    my $self = shift;

    return Zaaksysteem::Object::Types::Role->new(
        role_id     => $self->id,
        name        => $self->name,
        description => $self->description // '',
        system_role => $self->system_role ? 1 : 0,
        id          => $self->uuid,
    );
}

=head2 clean_redis_for_related_subjects

Clean the Redis cache for the related subjects.

=cut

sub clean_redis_for_related_subjects {
    my $self = shift;
    my $rs = $self->subjects;
    while (my $s = $rs->next) {
        $s->del_redis_key;
    }
}


=head2 TO_JSON

Implements the automagic serialization to JSON via L<JSON/encode>.

=cut

sub TO_JSON {
    my $self    = shift;

    my $parent_id = $self->get_column('parent_group_id');

    return {
        $self->get_columns,
        table     => 'Roles',
        $parent_id ? (parent_id => $parent_id) : ()
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

