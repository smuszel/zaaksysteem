package Zaaksysteem::Controller::FirstLogin;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::FirstLogin - Controller for the "First Login" page

=head1 METHODS

=head2 base

Base for the other methods in this controller. Adds the relevant configuration
values to the stash, for the templates to use.

=cut

sub base : Chained('/') : PathPart('first_login') : CaptureArgs(0) {
    my ($self, $c) = @_;

    if ($c->user->is_sorted && $c->user->has_legacy_permission('gebruiker')) {
        $c->res->redirect($c->uri_for('/'));
        $c->detach();
    }

    # Put the 2 config variables with text on the stash
    for my $key (qw(first_login_intro first_login_confirmation)) {
        $c->stash->{$key} = $c->model('DB::Config')->get_value($key);
    }

    if ($c->req->params->{confirmed}) {
        my $properties = $c->user->properties;
        $properties->{preferred_group_id} = $c->req->params->{preferred_group_id};

        $c->user->update({ properties => $properties });

        my %update_user = map(
            { $_ => $c->req->params->{$_} } grep(
                { defined $c->req->params->{ $_ } }
                qw/
                    displayname
                    initials
                    givenname
                    sn
                    telephonenumber
                    mail
                /
            )
        );

        $c->user->update_user(\%update_user);

        ### Find all employees having the "gebruikersbeheerder" role
        my $usermanager_role = $c->model('DB::Roles')->search(
            {
                name            => 'Gebruikersbeheerder',
                system_role     => 1,
            }
        )->first;

        if ($usermanager_role) {
            for my $subject ($usermanager_role->subjects->all) {
                $c->model('DB::Message')->message_create(
                    {
                        event_type  => 'user/apply',
                        subject_id  => $subject->betrokkene_identifier,
                        message     => 'Nieuwe aanmelding zaaksysteem: "' . $c->user->displayname . '"',
                        data        => {
                            user_displayname     => $c->user->displayname,
                            user_identifier      => $c->user->betrokkene_identifier,
                            group_name       => $c->user->preferred_group->name,
                            group_id         => $c->user->preferred_group->id,
                        }
                    }
                );
            }
        }
    }
}

=head2 first_login

Path: /first_login

Show the "This is your first login, please specify your department" page.

=cut

sub first_login : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ou_entries} = $c->model('DB::Groups')->get_as_tree($c->stash);

    if ($c->req->params->{confirmed} || $c->user->preferred_group) {
        $c->stash->{template} = 'first_login/confirmation.tt';
    } else {
        $c->stash->{template}   = 'first_login/first_login.tt';
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
