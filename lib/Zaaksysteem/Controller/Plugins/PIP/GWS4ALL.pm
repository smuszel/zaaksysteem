package Zaaksysteem::Controller::Plugins::PIP::GWS4ALL;
use Moose;

use BTTW::Tools;
use Zaaksysteem::ZTT::Context::GWS4allJaaropgave;
use Zaaksysteem::ZTT::Context::GWS4allSpecificatie;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Plugins::PIP::GWS4ALL - API calls for GWS4all from the PIP

=head1 METHODS

=head2 gws_base

Base controller for the rest of the GWS4all controllers. Ensures the GWS4all
interface is enabled, and that the logged-in betrokkene is a "natuurlijk
persoon".

=cut

sub gws_base : Chained('/plugins/pip/base') : PathPart('gws4all') : CaptureArgs(0) {
    my ($self, $c, $feature) = @_;

    my $res = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );
    if ($res->btype ne 'natuurlijk_persoon') {
        throw('gws4all/not_person', 'No active GWS4all interface found');
    }
    $c->stash->{bsn} = $res->burgerservicenummer;

    if (!$c->stash->{gws4all_interface}) {
        throw('gws4all/no_active_interface', 'No active GWS4all interface found');
    }
}

=head2 jaaropgave_base

Base controller for C<jaaropgave> and C<jaaropgave_pdf>, that retrieves the
relevant data from GWS4all.

=cut
define_profile jaaropgave_base => (
    required => {
        year => 'Int',
    },
);

sub jaaropgave_base : Chained('gws_base') : PathPart('jaaropgave') : CaptureArgs(0) {
    my ($self, $c) = @_;
    my $params = assert_profile($c->req->params)->valid;

    my $interface = $c->stash->{gws4all_interface};

    my $res = $interface->process_trigger(
        'get_jaaropgave',
        {
            year => $params->{year},
            bsn  => $c->stash->{bsn},
        },
    );

    $c->stash->{zapi} = [$res];
}

=head2 jaaropgave

Retrieve a "jaaropgave" from the GWS interface and return it in ZAPI.

=head3 Parameters

=over

=item * year

The year to retrieve the report for.

=back

=head3 URL

C</pip/gws4all/jaaropgave>

=cut

sub jaaropgave : Chained('jaaropgave_base') : PathPart('') {
}

=head2 jaaropgave_pdf

Retrieve a "jaaropgave" from the GWS interface and return it in ZAPI.

=head3 Parameters

=over

=item * index

Index of the jaaropgave to get a PDF off. This index is the (0-based) index in
the 'Jaaropgave' array, as returned by the C<jaaropgave> endpoint.

=back

This method is chained to C<jaaropgave>, so its parameters are required as well.

=head3 URL

C</pip/gws4all/jaaropgave_pdf>

=cut

define_profile jaaropgave_pdf => (
    required => {
        index => 'Int',
    },
);

sub jaaropgave_pdf : Chained('jaaropgave_base') : PathPart('pdf') {
    my ($self, $c) = @_;
    my $params = assert_profile($c->req->params)->valid;

    my $data = $c->stash->{zapi};

    if (!$data || !$data->[0] || !$data->[0]{JaarOpgave}[ $params->{index} ]) {
        throw('gws4all/pdf_no_data', "Can't generate PDF: no data");
    }

    my $context = Zaaksysteem::ZTT::Context::GWS4allJaaropgave->new(
        jaaropgave => $data->[0]{JaarOpgave}[ $params->{index} ],
        client     => $data->[0]{Client}
    );

    my $sjabloon_id = $c->stash->{gws4all_interface}->jpath('$.jaaropgave_sjabloon')->{id};
    my $template = $c->model('DB::BibliotheekSjablonen')->find($sjabloon_id);

    $c->res->content_type('application/pdf');
    $c->res->body(
        $c->model('PDFGenerator')->generate_pdf(
            context  => $context,
            template => $template,
        ),
    );
}

=head2 specificatie_base

Base controller for GWS4all "uitkeringsspecificaties".

=head3 Parameters

=over

=item * period

The period to retrieve the report for.

=back

=cut

define_profile specificatie_base => (
    required => {
        period => 'Int',
    },
);

sub specificatie_base : Chained('gws_base') : PathPart('specificatie') : CaptureArgs(0) {
    my ($self, $c) = @_;
    my $params = assert_profile($c->req->params)->valid;

    my $interface = $c->stash->{gws4all_interface};

    my $res = $interface->process_trigger(
        'get_specificatie',
        {
            bsn    => $c->stash->{bsn},
            period => $params->{period},
        },
    );

    $c->stash->{zapi} = [$res];
}

=head2 specificatie

Retrieve a "uitkeringsspecificatie" from the GWS interface and return it in
ZAPI format.

Chained to C<specificatie_base>, so look there for required parameters.

=head3 URL

C</pip/gws4all/specificatie>

=cut

sub specificatie : Chained('specificatie_base') : PathPart('') : ZAPI {
}

=head2 specificatie_pdf

Retrieve a "uitkeringsspecificatie" from the GWS interface and return it in
PDF format.

Chained to C<specificatie_base>, so look there for required parameters.

=head3 URL

C</pip/gws4all/specificatie>

=cut

define_profile specificatie_pdf => (
    required => {
        index => 'Int',
    },
);

sub specificatie_pdf : Chained('specificatie_base') : PathPart('pdf') {
    my ($self, $c) = @_;
    my $params = assert_profile($c->req->params)->valid;

    my $data = $c->stash->{zapi};

    if (!$data || !$data->[0] || !$data->[0]{Uitkeringsspecificatie}[ $params->{index} ]) {
        throw('gws4all/pdf_no_data', "Can't generate PDF: no data");
    }

    my $context = Zaaksysteem::ZTT::Context::GWS4allSpecificatie->new(
        specificatie => $data->[0]{Uitkeringsspecificatie}[ $params->{index} ],
        client     => $data->[0]{Client}
    );

    my $sjabloon_id = $c->stash->{gws4all_interface}->jpath('$.specificatie_sjabloon')->{id};
    my $template = $c->model('DB::BibliotheekSjablonen')->find($sjabloon_id);

    $c->res->content_type('application/pdf');
    $c->res->body(
        $c->model('PDFGenerator')->generate_pdf(
            context  => $context,
            template => $template,
        ),
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
