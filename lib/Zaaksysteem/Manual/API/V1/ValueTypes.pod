=head1 NAME

Zaaksysteem::Manual::API::V1::ValueTypes - Listing of all value types used by
the API

=head1 VALUE TYPES

This document lists the types of values and their interpretation standards
used by the Zaaksysteem API.

All listed entries come with an example of how the value is serialized by
Zaaksysteem.

=head2 Core types

=head3 string

    "string value"

The plain string type.

=head3 boolean

    true
    false

The plain boolean type.

=head3 text

    "text value"

Functionally similar to L</string>, except that it is implied that the text
can be, or will be, shown directly in user-facing UI.

=head3 serial

    1
    123

The serial type is used for the conceptual sequential identifiers used in
some contexts (L<C<case>|Zaaksysteem::Manual::API::V1::Case/number> for
example).

=head3 size

    123

A value type for the generalized concept of a set's cardinality.

Valid values for this type are member of the set of non-negative integers.

=head3 measure

    1
    1.5
    0.00000001

A value type for the generalized concept of an object's dimensions in some
arbitrary unit (for example physical length in meters, volume in liters, or
growth-rates).

Valid values for this type are member of the set of non-negative reals.

=head3 uuid

    "de4cc09d-ee2b-406f-821c-15f5b98aaad4"

A "version 4" UUID, as described in L<RFC 4122|https://tools.ietf.org/html/rfc4122>

=head3 reference

    {
        "type": "my_type",
        "reference": "c3212e4f-4643-4886-9adc-165255225c77"
    }

The reference value, used to define 'has a' relations between objects.

=head3 complex

    {
        "arbitrary": "foo",
        "keys": [
            "nested",
            {
                "as": "desired"
            }
        ]
    }

Complex values can be any valid JSON object. Aside from assuring that the
value is at the root level a JSON object, none of the content of the object
is validated generically.

=head2 Date and time values

All date(time) values must comply with
L<RFC 3339|https://tools.ietf.org/html/rfc3339> specifications. This
specification is based on a subset of the ISO-8601 timestamp, with simplified
semantics for use on the Internet.

Zaaksysteem further expects all timestamps to be in the UTC/zulu timezone.

=head3 date

    "2016-09-24"

=head3 datetime

    "2016-09-24T17:23:12Z"

=head3 duration

    "P4Y"             // 4 years
    "P5D"             // 5 days
    "P1Y2M3D4H5M6S"   // 1 year, 2 months, 3 days, 4 hours, 5 minutes, and 6 seconds

=head2 Case-related types

Historically the main production of Zaaksysteem have been the 'case' objects.
In order to support historic data, these case-specific attribute/value types
exist.

=head2 archival_type

    "preserve"
    "destroy"
    "transfer"

Enumeration of archival strategies. C<transfer> is available for historic
cases and should not be used in new registration.

=head2 dossier_type

    "fysical"
    "digital"

Enumeration of the two 'file' types. By default Zaaksysteem only produces
C<digital> files.

=head2 result

    "aangehouden"
    "aangekocht"
    "aangesteld"

Predefined enumeration of generic 'result' types of a case 'file'.

=head2 Workflow-related types

Value types related to workflows.

=head3 sequence_number

    1, 2, 3, ...

This value type abstracts the notion of a numbered sequence. The type is
composed L<Int|Moose::Manual::Types> values, under the constraint they are
natural/counting numbers ( > 0 ).

=head2 Location values

The C<location> values are used in multiple contexts, and consist mostly of
value literals lifted from older subsystems of Zaaksysteem. While their use
is currently required in, for example, the
L<case attribute update|Zaaksysteem::Manual::API::V1::Case/update> API, the
types will become deprecated in due time. It is recommended to ignore these
fields unless absolutely necessary.

=head3 bag_adres, bag_adressen, bag_straat_adres, bag_straat_adressen

    "nummeraanduiding-0310200000712548"

BAG address values represent a specific addresses.

=head3 bag_openbareruimte, bag_openbareruimtes

    "openbareruimte-0310300000656695"

These value types represent specific streets.

=head3 googlemaps

    "Brinklaan 35, 1404 Bussum, Nederland"

Googlemaps values are stored as a human-readable location description (like a
full address, a named location, etc).

=head3 geolatlon

    "51.604939956369606,3.739857006387519"

This value type defines an exact location on Earth using latitude and
longitude values in decimal notation

=head2 Deprecated types

This section lists value types that exist due to legacy infrastructure.

=head3 betrokkene_identifier

    "betrokkene-medewerker-42"
    "betrokkene-natuurlijk_persoon-53423"

Legacy identifier for subjects. While some contextual information can be
derived from values of this type, it is not recommended to do so. The
supported reference identifier for subjects are L<uuid|/uuid>s.

=head3 calendar

    "2015-04-02T12:00:00.000+0000;1970-01-01T12:45:00.000+0000;13374"

This composite value type defines the start, duration and external identifier
of an appointment for attributes that integrate with QMatic.

=head3 calendar_supersaas

    "2015-07-08T06:00:00Z;2015-07-08T06:30:00Z;17405420"

This composite value type defines the start, end, and external identifier of
an appointment for attributes that integrate with SuperSaas.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
