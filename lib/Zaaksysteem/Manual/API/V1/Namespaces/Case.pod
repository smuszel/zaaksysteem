=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Case - Case API reference
documentation

=head1 NAMESPACE URL

    /api/v1/case

=head1 DESCRIPTION

This page documents the endpoints within the C<case> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<case|Zaaksysteem::Manual::API::V1::Types::Case> instances.

=head3 C<GET /[case:id]>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head3 C<GET /[case:id]/casetype>

Returns a L<casetype|Zaaksysteem::Manual::API::V1::Types::Casetype> instance.

=head3 C<POST /create>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head3 C<POST /[case:id]/update>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head3 C<POST /[case:id]/transition>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head3 C<POST /[case:id]/take>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head3 C<POST /[case:id]/reject>

Rejects a case and send it to the configured "redistribution"-team within your company. The "redistribution"
team can be setup from the "configuration" panel. When no "distribution"-team is setup, or the case is
not in a C<new> state, the call will fail with an error.

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance on success.

=head3 C<POST /prepare_file>

Returns a L<filebag|Zaaksysteem::Manual::API::V1::Types::Filebag> instance.

=head2 ACL endpoints

=head3 C<GET /[case:id]/acl>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<caseE<sol>acl|Zaaksysteem::Manual::API::V1::Types::Case::Acl> instances.

=head3 C<POST /[case:id]/acl/update>

=begin javascript

{
    "values": [
        {
            "capabilities" : [
               "read",
               "write"
            ],
            "entity_id" : "10050|20006",
            "entity_type" : "position",
            "scope" : "instance"
        },
        {
            "capabilities" : [
               "read",
            ],
            "entity_id" : "10051|20006",
            "entity_type" : "position",
            "scope" : "instance"
        }
    ]
}

=end javascript

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<caseE<sol>acl|Zaaksysteem::Manual::API::V1::Types::Case::Acl> instances.

=head2 Document endpoints

=head3 C<GET /[case:id]/documents>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<file|Zaaksysteem::Manual::API::V1::Types::File> instances.

=head3 C<GET /[case:id]/document/[file:id]>

Returns a L<file|Zaaksysteem::Manual::API::V1::Types::File> instance.

=head3 C<GET /[case:id]/document/[file:id]/download>

Returns a file as an attachment.

=head2 Relation endpoints

=head2 Queue endpoints

=head3 C<GET /[case:id]/queue>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<queue_item|Zaaksysteem::Manual::API::V1::Types::QueueItem> instances.

=head3 C<GET /[case:id]/queue/[queue_item:id]>

Returns a L<queue_item|Zaaksysteem::Manual::API::V1::Types::QueueItem>
instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
