=head1 NAME

Zaaksysteem::Manual::API::V1::ScheduledJob::Legacy - Legacy ScheduledJobs retreival via ZQL

=head1 Description

Get legacy scheduled jobs objects from Zaaksysteem.

Legacy scheduled jobs are scheduled jobs which are not to be found in the new "object" system.
Some older features of Zaaksysteem depend on this infrastructre.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/scheduled_job/legacy

Make sure you use the HTTP Method C<GET> for retrieving.
You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 list

   /api/v1/scheduled_job/legacy

This will list all the legacy scheduled jobs in the system

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-f3b50f",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "total_rows" : 96,
            "rows" : 10,
            "next" : "https://sprint.zaaksysteem.nl/api/v1/scheduled_job/legacy?page=2",
            "page" : 1,
            "pages" : 10,
            "prev" : null
         },
         "rows" : [
            {
               "type" : "scheduled_job",
               "reference" : "abe3e335-708b-49c7-ae14-592829c8c4ec",
               "instance" : {
                  "deleted" : null,
                  "uuid" : "abe3e335-708b-49c7-ae14-592829c8c4ec",
                  "parameters" : {
                     "case_id" : 109,
                     "value" : "0",
                     "reason" : "",
                     "bibliotheek_kenmerken_id" : "1477"
                  },
                  "created" : null,
                  "schedule_type" : "manual",
                  "scheduled_for" : null,
                  "last_modified" : null,
                  "task" : "case/update_kenmerk",
                  "case" : {
                     "reference" : "c428d9e4-b166-412f-8746-ac6f3672cc31",
                     "type" : "case"
                  }
               }
            },
            {
               "type" : "scheduled_job",
               "reference" : "1a48ccb7-0fde-46e0-a9c0-a00bfb08716a",
               "instance" : {
                  "uuid" : "1a48ccb7-0fde-46e0-a9c0-a00bfb08716a",
                  "deleted" : null,
                  "created" : null,
                  "parameters" : {
                     "bibliotheek_kenmerken_id" : "1473",
                     "reason" : "",
                     "value" : "",
                     "case_id" : 109
                  },
                  "scheduled_for" : null,
                  "schedule_type" : "manual",
                  "task" : "case/update_kenmerk",
                  "last_modified" : null,
                  "case" : {
                     "reference" : "c428d9e4-b166-412f-8746-ac6f3672cc31",
                     "type" : "case"
                  }
               }
            }
         ]
      }
   }
}

=end javascript

=head2 get

    /api/v1/scheduled_job/legacy/abe3e335-708b-49c7-ae14-592829c8c4ec

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-c39769",
   "development" : false,
   "result" : {
      "instance" : {
         "case" : {
            "reference" : "c428d9e4-b166-412f-8746-ac6f3672cc31",
            "type" : "case"
         },
         "created" : null,
         "deleted" : null,
         "last_modified" : null,
         "parameters" : {
            "bibliotheek_kenmerken_id" : "1477",
            "case_id" : 109,
            "reason" : "",
            "value" : "0"
         },
         "schedule_type" : "manual",
         "scheduled_for" : null,
         "task" : "case/update_kenmerk",
         "uuid" : "abe3e335-708b-49c7-ae14-592829c8c4ec"
      },
      "reference" : "abe3e335-708b-49c7-ae14-592829c8c4ec",
      "type" : "scheduled_job"
   },
   "status_code" : 200
}

=end javascript

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
