/*global angular,_*/
(function ( ) { 
	
	angular.module('Zaaksysteem.case')
		.directive('zsCasePhaseView', [ function ( ) {
			
			return {
				require: [ 'zsCasePhaseView', '^zsCaseView' ],
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					
					var ctrl = this,
						zsCaseView,
						attrControls = [];
					
					ctrl.getPhaseId = function ( ) {
						return $attrs.zsCasePhaseId;	
					};

					ctrl.getPhaseName = function ( ) {
						return $attrs.zsCasePhaseName;
					};
					
					ctrl.getMilestone = function ( ) {
						return $attrs.zsCaseMilestone;	
					};
					
					ctrl.isLastMilestone = function ( ) {
						return zsCaseView.getNumMilestones() === ctrl.getMilestone();
					};
					
					ctrl.setControllers = function ( ) {
						zsCaseView = arguments[0];
						zsCaseView.addPhaseControl(ctrl);
						
						$scope.$on('$destroy', function ( ) {
							zsCaseView.removePhaseControl(ctrl);
						});
					};
					
					ctrl.addAttrControl = function ( ctrl ) {
						attrControls.push(ctrl);
					};
					
					ctrl.removeAttrControl = function ( ctrl ) {
						_.pull(attrControls, ctrl);
					};
					
					ctrl.getAttributeNames = function ( ) {
						return _.map(attrControls, function ( ctrl ) {
							return ctrl.getAttributeName();
						});
					};

					ctrl.isValid = function ( ) {
						return _.every(attrControls, function ( field ) {
							return field.isValid();
						});
					};
					
					return ctrl;
					
				}],
				controllerAs: [ 'casePhaseView' ],
				link: function ( scope, element, attrs, controllers ) {
					
					var zsCasePhaseView = controllers[0];
					
					zsCasePhaseView.setControllers.apply(zsCasePhaseView, controllers.slice(1));
					
				}
			};
			
			
		}]);
	
})();
