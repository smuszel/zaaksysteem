/*global angular*/
angular
	.module('Zaaksysteem.docs', [
		'Zaaksysteem.events',
		'Zaaksysteem.net',
		'Zaaksysteem.dom',
		'Zaaksysteem.message',
		'Zaaksysteem.directives',
		'Zaaksysteem.filters',
		'Zaaksysteem.core.user',
		'Zaaksysteem.core.data'
	]);
