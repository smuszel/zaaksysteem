/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsBeforeUnload', [ '$window', '$parse', function ( $window, $parse ) {
			
			var addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = window.zsFetch('nl.mintlab.utils.events.removeEventListener');
			
			return {
				link: function ( scope, element, attrs ) {
					
					var evtType = 'beforeunload';
					
					function onUnload ( event ) {
						$parse(attrs.zsBeforeUnload)(scope, { '$event': event });
					}
					
					if(attrs.zsBeforeUnload) {
						addEventListener($window, evtType, onUnload);
						scope.$on('$destroy', function ( ) {
							removeEventListener($window, evtType, onUnload);
						});
					}
					
				}
			};
			
		}]);
		
	
})();
