/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsScope', [ function ( ) {
			
			return {
				scope: true
			};
			
		}]);
	
})();