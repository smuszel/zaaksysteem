/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('zqlEscape', function ( ) {
			return function ( val ) {
				if(_.isString(val)) {
					val = '"' + val.replace(/\\/g, '\\\\').replace(/\"/g, '\\"') + '"';
				}
				return val;
			};
		});
})();