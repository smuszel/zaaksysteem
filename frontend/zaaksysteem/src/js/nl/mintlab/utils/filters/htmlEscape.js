/*global angular,_*/
var replaceValues = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#039;'
};
var re = new RegExp(Object.keys(replaceValues).join("|"),"gi");

(function ( ) {
  angular.module('Zaaksysteem')
    .filter('htmlEscape', function ( ) {

      return function ( val ) {
        if(_.isString(val)) {
          val = val.replace(re, function(matched) {
            return replaceValues[matched];
          });
        }
        return val;
      };
    });
})();