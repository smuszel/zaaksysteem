/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.service('subjectService', [ '$rootScope', 'smartHttp', function ( $rootScope, smartHttp ) {
			
			var subjectService = {},
				subject;
			
			subjectService.setSubject = function ( subj ) {
				var old = subject;
				subject = subj;
				$rootScope.$broadcast('subject.change', subject, old);
			};
			
			subjectService.enableSession = function ( identifier ) {
				return smartHttp.connect({
					method: 'POST',
					url: '/betrokkene/enable_session/',
					data: {
						identifier: identifier
					}
				})
					.success(function ( response ) {
						subjectService.setSubject(response.result[0]);
					});
			};
			
			subjectService.disableSession = function ( ) {
				var current = subject;
				
				subjectService.setSubject(null);
				
				return smartHttp.connect({
					method: 'POST',
					url: '/betrokkene/disable_session/'
				})	
					.error(function ( ) {
						subjectService.setSubject(current);
					});
			};
			
			subjectService.getSubject = function ( ) {
				return subject;
			};
			
			var activeSubjectUnwatch = $rootScope.$watch('subject', function ( activeSubject ) {
				if(activeSubject !== undefined) {
					subjectService.setSubject(activeSubject);
					activeSubjectUnwatch();
				}
			});
			
			return subjectService;
		
		}]);
	
})();